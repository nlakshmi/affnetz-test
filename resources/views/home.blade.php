@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">Employee Timesheet</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="col-sm-2 ng-binding">Week:</div>
                    <div class="col-sm-2 ng-binding">Week Start:</div>
                    <div class="col-sm-2 ng-binding">Week End:</div>
                    <button type="button" style="float:right;margin-bottom:10px;" class="btn btn-primary col-sm-1" id="addRow">Add Row</button>
                    <form id="timesheet-form" method="POST" action="{{ url('/home') }}" class="ng-pristine ng-valid ng-valid-min ng-valid-max">
                        @csrf
                    <table id="mytable" class="table table-bordered">
                      <thead>
                        <tr><th>SNO</th>
                        <th>Client</th>
                        <th>Work/OoO Type</th>
                        <th>Mon</th>
                        <th>Tue</th>
                        <th>Wed</th>
                        <th>Thu</th>
                        <th>Fri</th>
                        <th>Sat</th>
                        <th>Sun</th>
                      </tr></thead>
                      <tbody id="appendRow">
                        <?php $i = 1; ?>
                        @foreach($timesheet as $row)
                            <tr>
                                <td>{{ $i }}</td>
                                <td><select name="client[]">
                                    @foreach($clients as $client)
                                        <option value="{{$client->id}}" {{ $client->id ==  $row->client_id ? 'selected=selected' : ''}}>{{ $client->client_name }}</option>
                                    @endforeach
                                </select></td>
                                <td>{{ $row->week_start }}</td>
                                <td>{{ $row->d1_hours }}</td>
                                <td>{{ $row->d2_hours }}</td>
                                <td>{{ $row->d3_hours }}</td>
                                <td>{{ $row->d4_hours }}</td>
                                <td>{{ $row->d5_hours }}</td>
                                <td>{{ $row->d6_hours }}</td>
                                <td>{{ $row->d7_hours }}</td>
                            </tr>
                            <?php $i = $i+1; ?>
                        @endforeach
                      </tbody>
                  </table>
                <input type="submit" class="btn btn-primary" value="Save">
                <input type="button" class="btn btn-primary" value="Submit">
            </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.js" integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk=" crossorigin="anonymous"></script>
<script type="text/javascript">
    $(document).ready(function (){
        var clients = {!! json_encode($clients) !!}
        var rowIdx = 1;
        $('#addRow').click(function (){
            var size = $('#mytable >tbody >tr').length;
            var html = '';
            html += '<tr id='+(size+1)+'><td class="row-index text-center"><p>'+(size+1)+'</p></td>'
            html += '<td><select name="client[]" class="form-control form-control-sm"><option value="1">C1</option><option value="2">C2</option><option value="3">C3</option><option value="4">C4</option></select></td>'
            html += '<td><input name="work[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="mon[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="tue[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="wed[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="thu[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="fri[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="sat[]" type="text" class="form-control form-control-sm"></td>'
            html += '<td><input name="sun[]" type="text" class="form-control form-control-sm"></td></tr>'
            $('#appendRow').append(html);
        });
    });
</script>
@endsection

