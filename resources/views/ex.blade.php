<div ng-app="myApp" ng-controller="MainController" class="ng-scope">

  <h2 style="color:#428bca">Employee Timesheet</h2>
    <div class="row">
      <div class="col-sm-2 ng-binding">Week:</div>
      <div class="col-sm-2 ng-binding">Week Start:</div>
      <div class="col-sm-2 ng-binding">Week End:</div>
      <button type="button" style="float:right;margin-bottom:10px;" class="btn btn-primary col-sm-1" ng-click="addTimesheetRow()">Add Row</button>
    </div>
    <div class="row">
      <div class="col-md-12">
        <div class="table-responsive">
          <form id="timesheet-form" ng-submit="submitTimesheet()" class="ng-pristine ng-valid ng-valid-min ng-valid-max">
            <table id="mytable" class="table table-bordered">
              <thead>
                <tr><th>SNO</th>
                <th>Client</th>
                <th>Work/OoO Type</th>
                <th>Mon</th>
                <th>Tue</th>
                <th>Wed</th>
                <th>Thu</th>
                <th>Fri</th>
                <th>Sat</th>
                <th>Sun</th>
              </tr></thead>
              <tbody>
                <!-- ngRepeat: record in timesheets --><tr ng-repeat="record in timesheets" class="ng-scope">
                  <td class="ng-binding">1</td>
                  <td>
                    <select name="client0" ng-model="record.client" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty" ng-options="x for (x, y) in clients" disabled="disabled"><option label="Select Client" value="string:">Select Client</option><option label="C1" value="number:1" selected="selected">C1</option><option label="C2" value="number:2">C2</option></select>
                  </td>
                  <td>
                    <input name="work0" ng-model="record.work" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm col-sm-2 ng-pristine ng-untouched ng-valid ng-not-empty" disabled="disabled">
                  </td>
                  <td>
                    <input name="d10" type="number" min="0" max="24" ng-model="record.d1" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d20" type="number" min="0" max="24" ng-model="record.d2" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td><td>
                    <input name="d30" type="number" min="0" max="24" ng-model="record.d3" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d40" type="number" min="0" max="24" ng-model="record.d4" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d50" type="number" min="0" max="24" ng-model="record.d5" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d60" type="number" min="0" max="24" ng-model="record.d6" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d70" type="number" min="0" max="24" ng-model="record.d7" ng-disabled="!enabledEdit[0]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <div class="buttons">
                      <button class="btn btn-primary" ng-click="editTimesheetRow($index)">Edit</button>
                      <button class="btn btn-danger" ng-click="deleteTimesheetRow($index)">Delete</button>
                    </div>
                  </td>
                </tr><!-- end ngRepeat: record in timesheets --><tr ng-repeat="record in timesheets" class="ng-scope">
                  <td class="ng-binding">2</td>
                  <td>
                    <select name="client1" ng-model="record.client" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty" ng-options="x for (x, y) in clients" disabled="disabled"><option label="Select Client" value="string:">Select Client</option><option label="C1" value="number:1" selected="selected">C1</option><option label="C2" value="number:2">C2</option></select>
                  </td>
                  <td>
                    <input name="work1" ng-model="record.work" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm col-sm-2 ng-pristine ng-untouched ng-valid ng-not-empty" disabled="disabled">
                  </td>
                  <td>
                    <input name="d11" type="number" min="0" max="24" ng-model="record.d1" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d21" type="number" min="0" max="24" ng-model="record.d2" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td><td>
                    <input name="d31" type="number" min="0" max="24" ng-model="record.d3" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d41" type="number" min="0" max="24" ng-model="record.d4" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d51" type="number" min="0" max="24" ng-model="record.d5" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d61" type="number" min="0" max="24" ng-model="record.d6" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <input name="d71" type="number" min="0" max="24" ng-model="record.d7" ng-disabled="!enabledEdit[1]" class="form-control form-control-sm ng-pristine ng-untouched ng-valid ng-not-empty ng-valid-min ng-valid-max" ng-change="updateTotal()" disabled="disabled">
                  </td>
                  <td>
                    <div class="buttons">
                      <button class="btn btn-primary" ng-click="editTimesheetRow($index)">Edit</button>
                      <button class="btn btn-danger" ng-click="deleteTimesheetRow($index)">Delete</button>
                    </div>
                  </td>
                </tr><!-- end ngRepeat: record in timesheets -->
              </tbody>
              <tfoot>
              <tr><td></td>
                  <td colspan="2">Total</td>
                  <td class="ng-binding">12</td>
                  <td class="ng-binding">8</td>
                  <td class="ng-binding">5</td>
                  <td class="ng-binding">7</td>
                  <td class="ng-binding">1</td>
                  <td class="ng-binding">16</td>
                  <td class="ng-binding">11</td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
            <input type="button" class="btn btn-primary" value="Save">
            <input type="button" class="btn btn-primary" value="Submit">
          </form>
          <div class="clearfix"></div>
        </div>
      </div>
    </div>
</div>
