<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmployeeTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_timesheet', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('employee_id')->references('id')->on('employees');
            $table->unsignedBigInteger('client_id')->references('id')->on('clients');
            $table->integer('work_year');
            $table->integer('week_num');
            $table->date('week_start');
            $table->decimal('d1_hours','5','2')->nullable();
            $table->decimal('d2_hours','5','2')->nullable();
            $table->decimal('d3_hours','5','2')->nullable();
            $table->decimal('d4_hours','5','2')->nullable();
            $table->decimal('d5_hours','5','2')->nullable();
            $table->decimal('d6_hours','5','2')->nullable();
            $table->decimal('d7_hours','5','2')->nullable();
            $table->timestamps();
            $table->dateTime('submitted_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('employee_timesheet');
    }
}
