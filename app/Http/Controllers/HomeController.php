<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Client;
use App\EmployeeTimesheet;
use Cache;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $timesheet = Cache::remember('timesheet',15, function(){
            return EmployeeTimesheet::all();
        });
        $clients = Client::all();
        return view('home',compact('clients','timesheet'));
    }

    public function submit(Request $request)
    {
        foreach ($request->client as $key => $value) {
            $sheet = new EmployeeTimesheet;
            $sheet->employee_id = auth()->user()->id;
            $sheet->client_id = $value;
            $sheet->work_year = date("Y");
            $sheet->week_start = '2022-05-17';
            $sheet->week_num = $request->work[$key];
            $sheet->d1_hours = $request->mon[$key];
            $sheet->d2_hours = $request->tue[$key];
            $sheet->d3_hours = $request->wed[$key];
            $sheet->d4_hours = $request->thu[$key];
            $sheet->d5_hours = $request->fri[$key];
            $sheet->d6_hours = $request->sat[$key];
            $sheet->d7_hours = $request->sun[$key];
            $sheet->save();
        }       
        return redirect()->to('home');
    }
}
